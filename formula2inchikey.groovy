#!/usr/bin/groovy

import groovy.json.JsonSlurper

formulaFile = args[0]

println "formula file: $formulaFile"

jsonSlurper = new JsonSlurper()

cidBatchSize = 10

def downloadPubChemInfo(cidBatch, output) {
  url = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/${cidBatch}/property/InChIKey,MonoisotopicMass,InChI/JSON"
  // url = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/${cidBatch}/property/InChIKey/TXT"
  output.append "# " + url + "\n"
  conn2 = new URL(url).openConnection()
  conn2.setRequestProperty( 'User-Agent', 'isbjørn' )
  conn2.setRequestProperty( 'Accept', 'application/json' )
  responseCode = conn2.responseCode
  if (responseCode == 200) {
    json = conn2.inputStream.text
    cidData = jsonSlurper.parseText(json)
    cidData.PropertyTable.Properties.each { hit ->
      cid = hit.CID
      inchi = hit.InChI
      key = hit.InChIKey
      pcmass = hit.MonoisotopicMass
      output.append "$range\t$mf\t$mass\t$cid\t$pcmass\t$key\t$inchi\n"
    }
  }
}

new File(formulaFile).eachLine { line ->
  data = line.split("\t")
  range = data[0].trim()
  mf = data[1].trim()
  mass = data[2].trim()
  inchisFound = false
  if (mf.length() != 0) {
    println "Processing mf: $mf"
    connection = new URL("https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/fastformula/$mf/cids/JSON")
        .openConnection()
    connection.setRequestProperty( 'User-Agent', 'isbjørn' )
    connection.setRequestProperty( 'Accept', 'application/json' )
    responseCode = connection.responseCode
    output = new File("inchikeys_M" + formulaFile.split("formulas_")[1] + "_$mf")
    if (responseCode == 200) {
      inchisFound = true
      output.append "# Formula $mf has these InChIKeys:\n"
      json = connection.inputStream.text
      pubchemData = jsonSlurper.parseText(json)
      cidBatch = ""
      batchCount = 0
      pubchemData.IdentifierList["CID"].each { cid ->
        println "CID: $cid"
        batchCount++
        cidBatch += cid
        if (batchCount == cidBatchSize) {
          batchCount = 0
          if (cidBatch.endsWith(",")) cidBatch.substring(0, cidBatch.length() - 1);
          // fetch the InChIKeys for this batch
          Random r = new Random()
          sleepTime = r.nextInt(50) + 50
          sleep(sleepTime)
          downloadPubChemInfo(cidBatch, output)
          cidBatch = ""
        } else {
          cidBatch += ","
        }
      }
      // output also whatevery is left 
      if (batchCount > 0) {
        if (cidBatch.endsWith(",")) cidBatch.substring(0, cidBatch.length() - 1);
        // fetch the InChIKeys for this batch
        Random r = new Random()
        sleepTime = r.nextInt(500) + 50
        sleep(sleepTime)
        downloadPubChemInfo(cidBatch, output)
      }
    } else {
      output.append "# Formula $mf has no InChIKeys in PubChem: HTTP $responseCode\n"
    }
  }
  
  // back up solution if no MFs match the mass
  if (inchisFound == false) {
    output = new File("inchikeys_none")
    output.append "# No formulas match this mass: $formulaFile\n"
  }
}
