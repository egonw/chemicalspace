#!/usr/bin/groovy

@Grab(group='org.openscience.cdk', module='cdk-core', version='2.1.1')
@Grab(group='org.openscience.cdk', module='cdk-silent', version='2.1.1')
@Grab(group='org.openscience.cdk', module='cdk-formula', version='2.1.1')

import org.openscience.cdk.config.Isotopes
import org.openscience.cdk.formula.MolecularFormulaRange
import org.openscience.cdk.silent.SilentChemObjectBuilder
import org.openscience.cdk.formula.MolecularFormulaGenerator
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator

mass = Integer.valueOf(args[0])
massMax = (double)mass
massMin = (double)(massMax - 1)

ifac = Isotopes.getInstance()

range = new MolecularFormulaRange()
range.addIsotope( ifac.getMajorIsotope("C"), 0, 50)
range.addIsotope( ifac.getMajorIsotope("H"), 0, 50)
range.addIsotope( ifac.getMajorIsotope("N"), 0, 50)
range.addIsotope( ifac.getMajorIsotope("O"), 0, 50)
range.addIsotope( ifac.getMajorIsotope("S"), 0, 50)
range.addIsotope( ifac.getMajorIsotope("P"), 0, 50)

generator = new MolecularFormulaGenerator(
  SilentChemObjectBuilder.getInstance(),
  massMin, massMax, range
)
mfSet = generator.getAllFormulas()
for (mf in mfSet) {
  println mass + "\t" +
          MolecularFormulaManipulator.getString(mf) + "\t" +
          MolecularFormulaManipulator.getTotalExactMass(mf)
}
