#!/usr/bin/env nextflow

params.min = 15
params.max = 17
massRanges = params.min..params.max

process iterateRanges {
  input:
  val mass from massRanges
  output:
  file 'formulas_*' into formulas

  script:
  """
  groovy /home/egonw/var/Projects/GitLab/chemicalSpace/mass2formula.groovy $mass > formulas_$mass
  """
}

process iterateFormulas {
  input:
  val formula from formulas
  output:
  file 'inchikeys_*' into inchikeys

  script:
  """
  groovy /home/egonw/var/Projects/GitLab/chemicalSpace/formula2inchikey.groovy $formula
  """  
}

inchikeys.subscribe { files ->
  for (file in files) {
    if (!file.toString().contains("none")) {
      println "InChIKey file: $file"
    }
  }
}
